#grid processor
import numpy as np

import cv2
import os
import sys
import math
import tflearn
import pickle
import string
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from gensim.models import Word2Vec

from sklearn.svm import SVC
from tflearn.layers.core import *
from tflearn.layers.conv import *
from tflearn.layers.normalization import *
from tflearn.layers.estimator import regression

from sklearn.cluster import DBSCAN
import time
from pre_processing import *
from datetime import datetime, date

model = []
heat_maps = []
centers = []
clf = pickle.load(open('clf.pkl' , 'rb'))
modl = Word2Vec.load('word2vec_model')


class heat_map:
    fires = []
    maxdist = 100
    base_time = 0
    last_update_center = 0
    last_update_remove = 0
    center = []
    ''' each fire contains lat, lng , time , proba'''
    def return_time(self):
        return time.time()
    def __init__(self):
        self.fires = []
        self.last_update_remove = self.return_time()
        self.last_update_center = self.return_time()
    
    
        
    def find_dist_time(self , time):
        return abs(self.return_time()-time)
    
    def valid_time(self):
        if self.find_dist_time(self.last_update_remove) < 600000:
            return True
        return False

    def get_fires(self):
        return self.fires


    def remove_useless_fires(self):
        tmp = []
        for fire in self.fires:
            if self.valid_time(self, fire[2]) :
                tmp.append(fire)
        self.fires = tmp
    
    def add_fire(self , fire):
        self.fires.append(fire)
    
    def find_dist(self , location , fire):
        return (location[0]-fire[0])**2 + (location[1]-fire[1])**2

    def find_proba(self , location):
        proba = 0
        close_fires =[]
        sum_of_proba = 0
        num_of_proba = 0
        for fire in self.fires:
            dist = self.find_dist(location , fire)
            if dist < self.maxdist:
                close_fires.append([dist , fire])
                sum_of_proba+=fire[3]
                num_of_proba+=1
                proba = max(proba , fire[3])
        return proba
        



def update_centers():
    fires = []
    X = []

    for row in heat_maps:
        for heat_map in row:
            for fire in heat_map.get_fires():
                fires.append(fire)
                X.append([fire[0], fire[1]])


    clustering = DBSCAN().fit(X)
    r = set()
    for label  in  clustering.labels_:
        r.add(label)
    centers.clear()
    size_centers = len(r)
    for ii in r:
        lat = 0
        lng = 0
        proba = 0
        for i , t in enumerate(clustering.labels_):
            if t == ii:
                lat+=fires[i][0]
                lng+=fires[i][1]
                proba+=1
        centers.append([lat/proba , lng/proba])
    

def alert():
    old_centers = centers
    update_centers()
    moves = []
    for center in centers:
        mn = 1000000000
        point = 0
        for i ,old in enumerate(old_centers):
            dist = (old[0]-center[0])**2 + (old[1]-center[1])**2
            if dist < mn:
                mn = dist
                point = i
        if dist > 0:
            moves.append([center , old_centers[point]])

    rois = []
    for move in moves:
        rois.append([move[0] , [move[0][0]+move[0][0]-move[1][0] , move[0][1]+move[0][1]-move[1][1]]])

    return rois

def add_fire(location, time, proba=0.1):
    lan = float(location[0])
    lon = float(location[1])
    fire = [lan, lon, time, proba]

    heat_maps[900+int(lan*10)][1800+int(lon*10)].add_fire(fire)

def find_proba(location):
    lan = float(location[0]) 
    lon = float(location[1])
    w =     heat_maps[900+int(lan*10)][1800+int(lon*10)].find_proba(location)
    # print(w)

def construct_firenet (x,y):

    network = tflearn.input_data(shape=[None, y, x, 3], dtype=tf.float32)

    network = conv_2d(network, 64, 5, strides=4, activation='relu')

    network = max_pool_2d(network, 3, strides=2)
    network = local_response_normalization(network)

    network = conv_2d(network, 128, 4, activation='relu')

    network = max_pool_2d(network, 3, strides=2)
    network = local_response_normalization(network)

    network = conv_2d(network, 256, 1, activation='relu')

    network = max_pool_2d(network, 3, strides=2)
    network = local_response_normalization(network)

    network = fully_connected(network, 4096, activation='tanh')
    network = dropout(network, 0.5)

    network = fully_connected(network, 4096, activation='tanh')
    network = dropout(network, 0.5)

    network = fully_connected(network, 2, activation='softmax')

    network = regression(network, optimizer='momentum',
                         loss='categorical_crossentropy',
                         learning_rate=0.001)
    model = tflearn.DNN(network, checkpoint_path='firenet',
                        max_checkpoints=1, tensorboard_verbose=2)

    return model


model = construct_firenet (224, 224)
model.load(os.path.join("models/FireNet", "firenet"),weights_only=True)


def init():
    
    for i in range(1800):
        r = []
        for j in range(3600):
            hp = heat_map()
            r.append(hp)
        heat_maps.append(r)
    # model = construct_firenet (224, 224)
    # model.load(os.path.join("models/FireNet", "firenet"),weights_only=True)


def insert_photo(location , photo , time ):
   
    img = cv2.imread(photo)

    small_frame = cv2.resize(img, (224, 224), cv2.INTER_AREA)
    output = model.predict([small_frame])

    res = output[0][0]
    print(res)
    if res>0.2: 
        add_fire(location , time , res)
        

def insert_video(location , video, time ):
    rows = 224
    cols = 224

    keepProcessing = True;

    video = cv2.VideoCapture('test.mp4')


    cnt = 0
    num = 0
    while (keepProcessing):
        cnt+=1
        start_t = cv2.getTickCount();


        ret, frame = video.read()
        if not ret:
            break
        if cnt%25 == 1:
            small_frame = cv2.resize(frame, (rows, cols), cv2.INTER_AREA)
            output = model.predict([small_frame])
            if output[0][0]>0.7:
                num+=1
                if(num >= 5):
                    break

    if num>=5:
        add_fire(location , time , 0.5)


def get_proba_txt(text):
    o  = preprocess([text])
    o = word2vec(o , modl)
    o = clf.predict_proba(o)
    return o[0][1]

def insert_twitter(location , time , text):
    e = get_proba_txt(text)
    if e > 0.2:
        add_fire(location , time , e)

def date_to_timestamp(date):
    datetime_object = datetime.strptime(date, '%Y-%m-%d %H:%M:%S').timestamp()
    return datetime_object

def insert_api(api_res):
    
    for l in api_res:
        add_fire([l['lat'] , l['lon']] , date_to_timestamp(l['datetime']) ,1)


init()
print('done')

# insert_api([{"lon":-107.36,"lat":24.36,"temp4":305.5,"temp11":294.5,"size":-9,"temp":-9,"ecosys":93,"flag":2,"sat":15,"yd":2018012,"time":0,"datetime":"2018-01-12 00:00:00"}
# ,{"lon":-98.78,"lat":24.05,"temp4":299.8,"temp11":291.6,"size":-9,"temp":-9,"ecosys":24,"flag":3,"sat":15,"yd":2018012,"time":0,"datetime":"2018-01-12 00:00:00"},
# {"lon":-98.78,"lat":24.05,"temp4":300.7,"temp11":291,"size":0.0195,"temp":657,"ecosys":24,"flag":0,"sat":15,"yd":2018012,"time":30,"datetime":"2018-01-12 00:30:00"},{"lon":-98.78,"lat":24.05,"temp4":299.1,"temp11":290,"size":0.0181,"temp":654,"ecosys":24,"flag":0,"sat":15,"yd":2018012,"time":100,"datetime":"2018-01-12 01:00:00"},{"lon":-102.1,"lat":20.2,"temp4":295.5,"temp11":285.3,"size":-9,"temp":-9,"ecosys":32,"flag":3,"sat":15,"yd":2018012,"time":100,"datetime":"2018-01-12 01:00:00"},{"lon":-107.36,"lat":24.36,"temp4":305.5,"temp11":294.5,"size":-9,"temp":-9,"ecosys":93,"flag":2,"sat":15,"yd":2018012,"time":0,"datetime":"2018-01-12 00:00:00"},{"lon":-98.78,"lat":24.05,"temp4":299.8,"temp11":291.6,"size":-9,"temp":-9,"ecosys":24,"flag":3,"sat":15,"yd":2018012,"time":0,"datetime":"2018-01-12 00:00:00"},{"lon":-98.78,"lat":24.05,"temp4":300.7,"temp11":291,"size":0.0195,"temp":657,"ecosys":24,"flag":0,"sat":15,"yd":2018012,"time":30,"datetime":"2018-01-12 00:30:00"},{"lon":-98.78,"lat":24.05,"temp4":299.1,"temp11":290,"size":0.0181,"temp":654,"ecosys":24,"flag":0,"sat":15,"yd":2018012,"time":100,"datetime":"2018-01-12 01:00:00"},{"lon":-102.1,"lat":20.2,"temp4":295.5,"temp11":285.3,"size":-9,"temp":-9,"ecosys":32,"flag":3,"sat":15,"yd":2018012,"time":100,"datetime":"2018-01-12 01:00:00"},{"lon":-104.01,"lat":20.8,"temp4":295.4,"temp11":284.5,"size":-9,"temp":-9,"ecosys":22,"flag":3,"sat":15,"yd":2018013,"time":100,"datetime":"2018-01-13 01:00:00"},{"lon":-107.35,"lat":24.27,"temp4":298.8,"temp11":290.1,"size":-9,"temp":-9,"ecosys":32,"flag":3,"sat":15,"yd":2018013,"time":141,"datetime":"2018-01-13 01:41:00"},{"lon":-98.78,"lat":24.05,"temp4":317,"temp11":289.9,"size":0.0661,"temp":659,"ecosys":24,"flag":0,"sat":15,"yd":2018015,"time":0,"datetime":"2018-01-15 00:00:00"},{"lon":-98.77,"lat":24.09,"temp4":305.5,"temp11":288,"size":0.0281,"temp":691,"ecosys":47,"flag":0,"sat":15,"yd":2018015,"time":30,"datetime":"2018-01-15 00:30:00"},{"lon":-98.78,"lat":24.05,"temp4":298.4,"temp11":288.9,"size":0.1973,"temp":464,"ecosys":24,"flag":0,"sat":15,"yd":2018015,"time":30,"datetime":"2018-01-15 00:30:00"},{"lon":-98.78,"lat":24.05,"temp4":306.5,"temp11":288,"size":0.0788,"temp":582,"ecosys":24,"flag":0,"sat":15,"yd":2018015,"time":100,"datetime":"2018-01-15 01:00:00"},{"lon":-98.82,"lat":24.04,"temp4":301,"temp11":287.1,"size":0.0865,"temp":542,"ecosys":24,"flag":0,"sat":15,"yd":2018015,"time":145,"datetime":"2018-01-15 01:45:00"},{"lon":-98.57,"lat":21.9,"temp4":319.4,"temp11":289.6,"size":0.1071,"temp":624,"ecosys":31,"flag":0,"sat":15,"yd":2018016,"time":0,"datetime":"2018-01-16 00:00:00"},{"lon":-103.07,"lat":23.54,"temp4":304.1,"temp11":285,"size":-9,"temp":-9,"ecosys":40,"flag":3,"sat":15,"yd":2018017,"time":0,"datetime":"2018-01-17 00:00:00"},{"lon":-103.1,"lat":23.53,"temp4":299.1,"temp11":281.4,"size":-9,"temp":-9,"ecosys":40,"flag":3,"sat":15,"yd":2018017,"time":100,"datetime":"2018-01-17 01:00:00"},{"lon":-103.1,"lat":23.53,"temp4":294.7,"temp11":282.7,"size":0.0585,"temp":538,"ecosys":40,"flag":0,"sat":15,"yd":2018017,"time":145,"datetime":"2018-01-17 01:45:00"}])

# find_proba([24.05 ,-98.79 ])
update_centers()
# print(centers)
# print(get_proba_txt('Had a blast at Goju, the set for Kyle Alexander’s new video, “Oh My God!” It’s going to be FIRE! #gojucrazy… https://t.co/bDUfcb9DIP'))
# print(get_proba_txt('I have to hold a fire extinguisher in case a car catches on fire next to me... Ayee.... #exciting'))
# print(get_proba_txt('I''m bullet-proof, nothing to lose, fire away, fire away'))

# imgs = [ '2.jpg' , '3.jpg' ,'4.jpg' , '5.jpg','6.jpg','7.jpg' , '8.jpg']

# for img in imgs:
#     insert_photo([24.05 ,-98.79 ] , img , time.time())

